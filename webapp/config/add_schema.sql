ALTER TABLE memos ADD INDEX user_created_at_idx(user, created_at);
ALTER TABLE memos ADD INDEX is_private_created_at_idx(user, created_at);

ALTER TABLE memos ADD COLUMN first_line varchar(255) NOT NULL;
